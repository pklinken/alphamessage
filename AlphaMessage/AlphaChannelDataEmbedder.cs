﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace AlphaMessage
{
    /// <summary>
    /// Provides functionality to embed and retrieve data into the alpha channel of an image file.
    /// Constructor will set up all the properties as well as convert the image type to BGRA32 if
    /// an alpha channel is not present.</summary>
    /// <remarks>Currently only supports saving to BGRA32 PNG files and may misbehave when used with
    /// filetypes other than PNG, testing pending.</remarks>
    public class AlphaChannelDataEmbedder : INotifyPropertyChanged
    {
        /// <summary>
        /// Defines the size of the header in <see cref="rawData"/></summary>
        private const int HeaderSize = 8;

        /// <summary>
        /// Contains user-entered data as well as a header describing its contents.
        /// Header format: 
        /// [0] AlphaChannelDataType enum
        /// [1-4] Size of data after header
        /// [5-7] File extension used when exporting binary data</summary>
        private byte[] rawData;

        /// <summary>
        /// Returns true if the loaded image contains embedded data in its alpha channel.
        /// </summary>
        public bool HasEmbeddedData
        {
            get
            {
                return DataType == DataTypeEnum.BinaryData || DataType == DataTypeEnum.BinaryDataEncrypted
                    || DataType == DataTypeEnum.PlainText || DataType == DataTypeEnum.PlainTextEncrypted;
            }
        }

        /// <summary>
        /// Returns true if the loaded image has embedded data that is encrypted
        /// </summary>
        public bool DataEncrypted
        {
            get { return ((DataTypeEnum)rawData[0] == DataTypeEnum.PlainTextEncrypted || (DataTypeEnum)rawData[0] == DataTypeEnum.BinaryDataEncrypted); }
        }

        /// <summary>
        /// Returns a human-readable string describing the contents of embedded data, or an empty string if none is present.
        /// </summary>
        public string HumanReadableDescription
        {
            get
            {
                string description = "";
                if (HasEmbeddedData)
                {
                    description += "Data type: ";
                    switch (DataType)
                    {
                        case DataTypeEnum.BinaryDataEncrypted:
                            description += "binary\n";
                            break;
                        case DataTypeEnum.PlainTextEncrypted:
                            description += "plain text\n";
                            break;
                        case DataTypeEnum.BinaryData:
                            description += "binary\n";
                            break;
                        case DataTypeEnum.PlainText:
                            description += "plain text\n";
                            break;
                    }
                    description += "Encrypted: " + (this.DataEncrypted ? "yes\n" : "no\n");
                    description += "Size: " + DataSize + " bytes\n";
                    description += "File extension: " + FileExtension + "\n\n";
                    if (DataType == DataTypeEnum.PlainText)
                    {
                        description += "Contents: " + DataAsString() + "\n";
                    }
                }
                else
                {
                    //description += "Image doesn't contain embedded data.";
                }

                return description;
            }
        }

        /// <summary>
        /// Returns the file extension of embedded binary data or .txt if the image contains plain text.
        /// </summary>
        public string FileExtension
        {
            get
            {
                if (DataType == DataTypeEnum.PlainText || DataType == DataTypeEnum.PlainTextEncrypted)
                    return ".txt";
                else
                {
                    char[] fileType = new char[4];
                    fileType[0] = '.';
                    fileType[1] = Convert.ToChar(rawData[5]);
                    fileType[2] = Convert.ToChar(rawData[6]);
                    fileType[3] = Convert.ToChar(rawData[7]);
                    return new String(fileType);
                }
            }
            private set
            {
                rawData[5] = Convert.ToByte(value[0]);
                rawData[6] = Convert.ToByte(value[1]);
                rawData[7] = Convert.ToByte(value[2]);
            }
        }

        /// <summary>
        /// Returns the user-data size field from the header.
        /// </summary>
        public int DataSize
        {
            get
            {
                int size = rawData[4];
                size |= rawData[3] << 8;
                size |= rawData[2] << 16;
                size |= rawData[1] << 24;
                return size;
            }
            private set
            {
                rawData[1] = (byte)(value >> 24);
                rawData[2] = (byte)((value >> 16) & 0xff);
                rawData[3] = (byte)((value >> 8) & 0xff);
                rawData[4] = (byte)(value & 0xff);
            }
        }

        /// <summary>
        /// Returns the type of data embedded in the loaded image as a <see cref="DataTypeEnum"/>
        /// </summary>
        public DataTypeEnum DataType
        {
            get { return (DataTypeEnum)rawData[0]; }
        }

        /// <summary>
        /// Enumeration of all possible embedded data types.
        /// </summary>
        public enum DataTypeEnum
        {
            PlainText = 0x45, BinaryData = 0xc2,
            PlainTextEncrypted = 0x46, BinaryDataEncrypted = 0xc3
        };

        /// <summary>
        /// Available space for user-entered data in the current image.</summary>
        public int AvailableSpace { get; private set; }

        /// <summary>
        /// The currently loaded image of an instance.
        /// </summary>
        public BitmapSource ImageSource { get; private set; }

        /// <summary>
        /// The height, width and stride of the currently loaded image.
        /// </summary>
        private int height, width, stride;
        //private BitmapMetadata metadata;

        /// <summary>
        /// Loads an image from the provided Uri, converts the image to a type with an alpha channel
        /// if necessary and sets up all the class properties.</summary>
        /// <param name="fileName">The filename of the image to load.</param>
        public AlphaChannelDataEmbedder(Uri fileName)
        {
            BitmapImage tempImage = new BitmapImage();
            tempImage.BeginInit();
            tempImage.UriSource = fileName;
            tempImage.EndInit(); 
            ImageSource = tempImage;
            
            //BitmapDecoder bd = BitmapDecoder.Create(fileName, BitmapCreateOptions.None, BitmapCacheOption.Default);
            //metadata = (BitmapMetadata) bd.Frames[0].Metadata;

            // Set the height, width and stride for the image
            height = ImageSource.PixelHeight;
            width = ImageSource.PixelWidth;
            stride = (width * ImageSource.Format.BitsPerPixel + 7) / 8;

            // Allocate space for potential payload
            // We store 1 bit per pixel in the alpha channel, but have to subtract the space
            // required by the header.
            int rawDataSize = (ImageSource.PixelHeight * ImageSource.PixelWidth / 8);
            rawData = new byte[rawDataSize];
            AvailableSpace = rawDataSize - HeaderSize;

            // If necessary, convert the image to a type with an alpha channel
            if (!HasAlphaChannel())
                ConvertToBitmapWithAlphaChannel();

            // Attempt to retrieve existing data from the alpha channel
            RetrieveDataFromBitmap();
        }

        /// <summary>
        /// Checks if the loaded image has an alpha channel.</summary>
        /// <returns>True is image has an alpha channel, false otherwise.</returns>
        private bool HasAlphaChannel()
        { // TODO: there are more filetypes with an alpha channel, notably 64/128 bit ones, ignoring those for now.
            PixelFormat pf = ImageSource.Format;
            return (pf == PixelFormats.Bgra32 || pf == PixelFormats.Pbgra32);
        }

        /// <summary>
        /// Converts the current image to a type that has an alpha channel.</summary>
        /// <remarks>Specifically, to a PNG RGBA32 image</remarks>
        private void ConvertToBitmapWithAlphaChannel()
        {
            // Copy the raw pixels to an array
            byte[] pixelArray = new byte[height * stride];
            ImageSource.CopyPixels(pixelArray, stride, 0);

            // Create a new Bitmap with an alpha channel from the raw pixels
            BitmapSource convertedBitmap = BitmapSource.Create(width, height, ImageSource.DpiX, ImageSource.DpiY,
                PixelFormats.Bgra32, ImageSource.Palette, pixelArray, stride);

            // Set our image source to the new Bitmap
            ImageSource = convertedBitmap;
        }

        /// <summary>
        /// Copies all data present in the alpha channel to rawData[].
        /// </summary>
        private void RetrieveDataFromBitmap()
        {
            // Copy the raw pixels to an array
            byte[] pixelArray = new byte[height * stride];
            ImageSource.CopyPixels(pixelArray, stride, 0);

            // Every 4th byte in pixelArray is an alpha channel and will (on a valid file)
            // contain either 0xff or 0xfe, corresponding to a bit in a byte element of rawData[]
            // The following algorithm retrieves this data.
            int pixelArrayIndex = 3;
            int dataIndex = 0;
            int bitIndex = 0;
            byte b = 0;
            while (pixelArrayIndex < pixelArray.Length)
            {
                if (bitIndex == 8)
                {
                    rawData[dataIndex++] = b;
                    bitIndex = 0;
                    b = 0;
                }
                b |= (byte)((pixelArray[pixelArrayIndex] == 0xff ? 0 : 1) << bitIndex);
                pixelArrayIndex += 4;
                bitIndex++;
            }
        }

        /// <summary>
        /// Embeds the contents of rawData[] in the alpha channel of the loaded image.
        /// </summary>
        private void EmbedDataInBitmap()
        {
            // Copy the raw pixels to an array
            byte[] pixelArray = new byte[height * stride];
            ImageSource.CopyPixels(pixelArray, stride, 0);

            // pixelArray now contains all the pixels from our bitmap
            // Every 4th byte in pixelArray is an alpha channel and will (on a valid file)
            // contain either 0xff or 0xfe, corresponding to a bit in a byte element of rawData[]
            // The following algorithm embeds rawData[] in this alpha channel.
            int pixelArrayIndex = 3;
            int dataIndex = 0;
            int bitIndex = 0;
            while (pixelArrayIndex < pixelArray.Length && dataIndex < rawData.Length)
            {
                int bit = (byte)(rawData[dataIndex] & (1 << bitIndex));
                pixelArray[pixelArrayIndex] = bit == 0 ? (byte)0xff : (byte)0xfe;
                bitIndex++;
                pixelArrayIndex += 4;
                if (bitIndex == 8)
                {
                    dataIndex++;
                    bitIndex = 0;
                }
            }
            // pixelArray now has rawData[] embedded in its alpha channel, create a new BitmapSource
            // using pixelArray
            BitmapSource embeddedBitmap = BitmapSource.Create(width, height, ImageSource.DpiX, ImageSource.DpiY, 
                PixelFormats.Bgra32, ImageSource.Palette, pixelArray, stride);

            ImageSource = embeddedBitmap;
        }

        
        /// <summary>
        /// Store plaintext data in rawData[] and update its header and the instance properties
        /// </summary>
        /// <param name="plainText">The plaintext data to store</param>
        /// <param name="password">An optional password to encrypt the plaintext</param>
        /// <remarks>Actual embedding into the loaded image is not done until the user saves the image
        /// <see cref="SaveImageToFile"/></remarks>
        public void EmbedString(string plainText, string password = null)
        {
            // Move the plainText to a byte array
            byte[] data = Encoding.UTF8.GetBytes(plainText);
            
            // If a password parameter was given, use it to encrypt the data
            if (password != null)
                data = AESGCM.SimpleEncryptWithPassword(data, password);
            
            // Encryption adds some overhead so check if the data will still fit
            if (data.Length > AvailableSpace)
                throw new PayloadTooLargeException("Message too large");

            // All is well, store result and new header in rawData and update other properties
            rawData[0] = password == null ? (byte)DataTypeEnum.PlainText : (byte)DataTypeEnum.PlainTextEncrypted;
            DataSize = data.Length;
            Array.Copy(data, 0, rawData, HeaderSize, data.Length);
            /* for (int index = 0; index < data.Length; index++)
            {
                rawData[index + HeaderSize] = data[index];
            } */
  
            updateProperties();
        }

        /// <summary>
        /// Store a file in rawData[] and update its header and the instance properties
        /// </summary>
        /// <param name="fileName">The name of the file to store</param>
        /// <param name="password">An optional password to encrypt the file</param>
        /// <remarks>Actual embedding into the loaded image is not done until the user saves the image
        /// <see cref="SaveImageToFile"/></remarks>
        public void EmbedFile(string fileName, string password = null)
        {
            byte[] data = File.ReadAllBytes(fileName);

            // If a password parameter was given, use it to encrypt the data
            if (password != null)
                data = AESGCM.SimpleEncryptWithPassword(data, password);

            // Encryption adds some overhead so check if the data will still fit
            if (data.Length > AvailableSpace)
                throw new PayloadTooLargeException("File too large");

            // All is well, store result and new header in rawData and update other properties
            rawData[0] = password == null ? (byte)DataTypeEnum.BinaryData : (byte)DataTypeEnum.BinaryDataEncrypted;
            DataSize = data.Length;
            FileExtension = fileName.Substring(fileName.Length - 3, 3);
            Array.Copy(data, 0, rawData, HeaderSize, data.Length);
            /* for (int index = 0; index < data.Length; index++)
            {
                rawData[index + HeaderSize] = data[index];
            } */

            updateProperties();

        }

        /// <summary>
        /// Returns the data portion of rawData[] as a string.
        /// </summary>
        private string DataAsString()
        {
            char[] dataAsChar = new char[DataSize];
            // FIXME: There is bound to be a .NET functions for this like Array.Copy/ConvertAll
            for (int index = 0; index < dataAsChar.Length; index++)
            {
                dataAsChar[index] = Convert.ToChar(rawData[index + HeaderSize]);
            }
            return new string(dataAsChar);
        }

        /// <summary>
        /// Save the loaded image to a file.
        /// </summary>
        /// <param name="fileName">The filename to which to save</param>
        internal void SaveImageToFile(string fileName)
        {
            // Embed rawData[] into the alpha channel of loaded image
            EmbedDataInBitmap();
            using (FileStream fileStream = new FileStream(fileName, FileMode.Create))
            {
                PngBitmapEncoder encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(ImageSource));
                //encoder.Frames[0].Metadata = metadata;
                //InPlaceBitmapMetadataWriter metaWriter = encoder.Frames[0].CreateInPlaceBitmapMetadataWriter();
                //encoder.Metadata = metadata;
                encoder.Save(fileStream);
            }
        }

        /// <summary>
        /// Save data embedded in loaded image to a file
        /// </summary>
        /// <param name="fileName">The file to save data to</param>
        /// <param name="password">Optional password</param>
        /// <exception cref="InvalidCipherException"/>
        internal void ExportDataToFile(string fileName, string password=null)
        {
            byte[] payLoad = new byte[DataSize];
           
            Array.Copy(rawData, HeaderSize, payLoad, 0, DataSize);

            // If encrypted, attempt to decrypt the data, raises an exception on failure
            if (DataEncrypted)
            {
                byte[] decryptedData = AESGCM.SimpleDecryptWithPassword(payLoad, password);
                if (decryptedData == null) 
                { // our encryption method will return null on a bad password 
                    throw new InvalidCipherException();
                }
                payLoad = decryptedData;
            }

            // Attempt to write payLoad to file
            using (FileStream fileStream = new FileStream(fileName, FileMode.Create))
            {
                fileStream.Write(payLoad, 0, payLoad.Length);
            }
        }

        /// <summary>
        /// Updates the public properties so the UI bindings update.
        /// </summary>
        /// <remarks>It would be preferable to do this in the setters of the properties
        /// but the way the design worked out this works OK.</remarks>
        private void updateProperties()
        {
            OnPropertyChanged("DataType");
            OnPropertyChanged("HumanReadableDescription");
            OnPropertyChanged("HasEmbeddedData");
            OnPropertyChanged("DataEncrypted");
        }

        public event PropertyChangedEventHandler PropertyChanged;
        
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }

    /// <summary>
    /// Exception thrown when a payload turns out to be too large to be embedded.
    /// Usually the UI logic would prevent the user from attempting to embed too large data,
    /// but encryption adds overhead that can cause data to become too large which is when you
    /// will see this exception.
    /// </summary>
    public class PayloadTooLargeException : Exception
    {
        public PayloadTooLargeException()
        {
        }

        public PayloadTooLargeException(string message) : base(message)
        {
        }

        public PayloadTooLargeException(string message, Exception inner) : base(message, inner)
        {
        }

    }

    /// <summary>
    /// Exception raised when the user enters an invalid password when attempting to export data to a file
    /// </summary>
    public class InvalidCipherException : Exception
    {
        public InvalidCipherException() { }
        public InvalidCipherException(string message) : base(message) { }
        public InvalidCipherException(string message, Exception inner) : base(message, inner) { }
    }
}
