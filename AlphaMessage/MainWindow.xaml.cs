﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AlphaMessage
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        /// <summary>
        /// Constructed when a user opens an image, its properties and methods do all the actual interesting stuff and also
        /// bind to many UI elements.
        /// </summary>
        public AlphaChannelDataEmbedder imageEmbedder;
        /// <summary>
        /// Used to remind the user to save his changes when exiting the app or closing an image.
        /// </summary>
        private bool changesMade = false;

        private int payloadSize;
        /// <summary>
        /// The size of the data the user has entered/selected, updates dynamically as the user
        /// enters text or selects a file.
        /// </summary>
        public int PayloadSize
        {
            get { return this.payloadSize; }
            set
            {
                if(this.payloadSize != value)
                {
                    this.payloadSize = value;
                    NotifyPropertyChanged("PayloadSize");
                    int availableSpace = imageEmbedder != null ? imageEmbedder.AvailableSpace : 0;
                    IsPayloadExceedSpace = value > availableSpace;
                }
            }
        }

        private bool isPayloadExceedSpace;
        /// <summary>
        /// True if <see cref="PayLoadSize"/> exceeds the available space in the loaded image.
        /// Value updated by PayloadSize setter. Used by the UI to change text colour when payload
        /// exceeds the available space.
        /// </summary>
        public bool IsPayloadExceedSpace
        {
            get { return this.isPayloadExceedSpace; }
            set
            {
                if(this.isPayloadExceedSpace != value)
                {
                    this.isPayloadExceedSpace = value;
                    NotifyPropertyChanged("IsPayloadExceedSpace");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String info)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(info));
        }
       
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Open_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        /// <summary>
        /// On receiving the Open command, presents the user with an OpenFileDialog and attempts to create an instance of
        /// <see cref=" AlphaChannelDataEmbedder"/> and set up all the UI controls.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Open_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image files (*.png;*.jpg;*.jpeg)|*.png;*.jpg;*.jpeg|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    imageEmbedder = new AlphaChannelDataEmbedder(new Uri(openFileDialog.FileName));
                }
                catch (System.NotSupportedException ex)
                {
                    MessageBox.Show(String.Format("Error opening image file: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                // TODO: Some filetypes seem to raise another exception which is currently not being caught

                // TODO: This can also be done as a binding
                imgMain.Stretch = Stretch.Uniform;
                imgMain.Source = imageEmbedder.ImageSource;

                // Set our window's DataContext to the AlphaChannelDataEmbedder instance so our XAML bindings work
                wnd.DataContext = imageEmbedder;
                changesMade = false;

                // Reset some UI elements
                tbBrowseFilename.Text = "";
                tbPlainText.Text = "";
                cbPassword.IsChecked = false;
                pbPassword.Password = "";
                pbValidation.Password = "";
                // TODO: Also select a particular tab based on the file having embedded data or not.
            }
        }

        /// <summary>
        /// Attempts to embed user-entered plaintext or a file into the loaded image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EmbedButton_Click(object sender, RoutedEventArgs e)
        {
            // Check if an image is loaded (TODO: Could also disable the button using a binding)
            if (imageEmbedder == null)
            {
                MessageBox.Show("Please open an image file.");
                return;
            }
            // Check wether the user wants to embed plain text or a file
            if ((bool)rbPlainText.IsChecked)
            {
                string plainText = tbPlainText.Text;
                string password = null;
                // Some validation checks
                if (plainText == "")
                {
                    MessageBox.Show("Please enter some text to embed.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                if (plainText.Length > imageEmbedder.AvailableSpace)
                {
                    MessageBox.Show("Message too large", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                if ((bool)cbPassword.IsChecked && ValidatePasswords())
                {
                    password = pbPassword.Password;
                }

                // Attempt to embed data, may still fail due to encryption overhead.
                try
                {
                    imageEmbedder.EmbedString(plainText, password);
                    changesMade = true;
                }
                catch (PayloadTooLargeException ex)
                {
                    MessageBox.Show("Error embedding message: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else if ((bool)rbFile.IsChecked)
            {
                string fileName = tbBrowseFilename.Text;
                string password = null;
                // Some validation checks
                if (fileName == "")
                {
                    MessageBox.Show("Please select a file", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                // TODO: Catch exceptions here
                FileStream saveFile = File.OpenRead(fileName);
                if (saveFile.Length > imageEmbedder.AvailableSpace)
                {
                    MessageBox.Show("File too large.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                saveFile.Close();
                if ((bool)cbPassword.IsChecked && ValidatePasswords())
                {
                    password = pbPassword.Password;
                }

                // Attempt to embed data, may still fail due to encryption overhead.
                try
                {
                    imageEmbedder.EmbedFile(fileName, password);
                    changesMade = true;
                }
                catch (PayloadTooLargeException ex)
                {
                    MessageBox.Show("Error embedding file: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        /// <summary>
        /// Checks if the password and password validation fields match and are not empty.
        /// </summary>
        /// <returns>True if passwords are valid and equal, false otherwise</returns>
        private bool ValidatePasswords()
        {
            string pass1, pass2;
            pass1 = pbPassword.Password;
            pass2 = pbValidation.Password;
            if (String.IsNullOrWhiteSpace(pass1))
            {
                MessageBox.Show("Cannot use empty password");
                return false;
            }
            if (pass1 != (pass2 ?? ""))
            {
                MessageBox.Show("Passwords do not match");
                return false;
            }

            return true;
        }

        private void SaveAs_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = (imageEmbedder != null);
        }

        /// <summary>
        /// On receiving the SaveAs command, presents the user with a SaveFileDialog and attempt to save an image and its embedded data to a file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveAs_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            // TODO: Add support for other image types
            // saveFileDialog.Filter = "Image files (*.png;*.jpg;*.jpeg)|*.png;*.jpg;*.jpeg|All files (*.*)|*.*";
            saveFileDialog.Filter = "Image files (*.png)|*.png";
            if (saveFileDialog.ShowDialog() == true)
            {
                try
                {
                    imageEmbedder.SaveImageToFile(saveFileDialog.FileName);
                    changesMade = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error saving image: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void ExitCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        /// <summary>
        /// On receiving the Exit command, checks if there are unsaved changes to show a confirmation dialog, otherwise just exits the application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExitCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        { // TODO: Check if any unsaved changes might be lost here
            if (changesMade)
            {
                MessageBoxResult result = MessageBox.Show("Any changes you made will be lost, exit anyway?", "Exit application", MessageBoxButton.YesNoCancel);
                if (result == MessageBoxResult.Yes)
                    Application.Current.Shutdown();
            }
            else
                Application.Current.Shutdown();

        }

        /// <summary>
        /// Opens an OpenFileDialog and on successful file selection updates the PayloadSize property
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BrowseButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image files All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
            {
                tbBrowseFilename.Text = openFileDialog.FileName;
                FileStream saveFile = File.OpenRead(tbBrowseFilename.Text);
                PayloadSize = (int)saveFile.Length;
                saveFile.Close();
            }
        }

        /// <summary>
        /// Opens a SaveFileDialog for the user to select a file, prompts the user for a password if necessary and
        /// on success attempts to export the current image's embedded data to a file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportButton_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            string fileName;

            // Suggest file extension based on data type and FileExtension property of loaded image.
            if (imageEmbedder.DataType == AlphaChannelDataEmbedder.DataTypeEnum.BinaryData
                || imageEmbedder.DataType == AlphaChannelDataEmbedder.DataTypeEnum.BinaryDataEncrypted)
            {
                saveFileDialog.Filter = "All files (*.*)|*.*";
                saveFileDialog.DefaultExt = imageEmbedder.FileExtension;
            }
            else
            {
                saveFileDialog.Filter = "Text files (*.txt)|*.txt";
                saveFileDialog.DefaultExt = imageEmbedder.FileExtension;
            }
            if (saveFileDialog.ShowDialog() == true)
                fileName = saveFileDialog.FileName;
            else
                return;

            string password = null;
            if (imageEmbedder.DataEncrypted)
            { // Data password protected, prompt the user for the password
                PasswordDialog passwordDialog = new PasswordDialog();
                if (passwordDialog.ShowDialog() == true)
                {
                    password = passwordDialog.Password;
                }
                else
                {
                    MessageBox.Show("Please enter a password to attempt data export", "Password required", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }

            // Attempt to export the data
            try
            {
                imageEmbedder.ExportDataToFile(fileName, password);
            }
            catch (System.IO.IOException ex)
            {
                MessageBox.Show("Error exporting data: " + ex.Message, "IO Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (System.ArgumentException ex)
            {
                MessageBox.Show("Error decrypting data: " + ex.Message, "Encryption error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (InvalidCipherException ex)
            {
                MessageBox.Show("Error deciphering data: password incorrect", "Password error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void CloseCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = imgMain.Source != null ? true : false;
        }

        /// <summary>
        /// On receiving the Close command, checks if there are unsaved changes to show a confirmation dialog, otherwise just removes all references to the image.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (changesMade)
            {
                MessageBoxResult result = MessageBox.Show("Any changes you made will be lost, close anyway?", "Close image", MessageBoxButton.YesNoCancel);
                if (result != MessageBoxResult.Yes)
                    return;
            }
            
            imgMain.Source = null;
            wnd.DataContext = null;
            imageEmbedder = null;
        }

        /// <summary>
        /// Update the PayloadSize property when the user changes the contents of tbPlainText
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbPlainText_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            // TODO: This can probably be done by a binding.
            PayloadSize = tb.Text.Length;
        }

        /// <summary>
        /// When the user switches between plain-text or file content in the Embed tab, update the PayloadSize property
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rbContentType_Checked(object sender, RoutedEventArgs e)
        {
            // Check in place to avoid exception on window load
            if (tbPlainText == null || tbSize == null || tbBrowseFilename == null)
                return;

            RadioButton rb = sender as RadioButton;
            if (rb.Name == "rbPlainText" && tbPlainText.Text.Length > 0)
            {
                PayloadSize = tbPlainText.Text.Length;
            }
            else if (tbBrowseFilename.Text != "")
            {
                // TODO: Catch exceptions
                FileStream saveFile = File.OpenRead(tbBrowseFilename.Text);
                PayloadSize = (int)saveFile.Length;
                saveFile.Close();
            }
            else
            {
                PayloadSize = 0;
            }
        }
    }

    /// <summary>
    /// Custom commands used in the application
    /// </summary>
    public static class CustomCommands
    {
        public static readonly RoutedUICommand Exit = new RoutedUICommand(
            "Exit", "Exit", typeof(CustomCommands), new InputGestureCollection()
            {
                new KeyGesture(Key.F4, ModifierKeys.Alt)
            });

        public static readonly RoutedUICommand Close = new RoutedUICommand(
            "Close", "Close", typeof(CustomCommands), new InputGestureCollection()
            {
                new KeyGesture(Key.Q, ModifierKeys.Control)
            });
    }

    /// <summary>
    /// Converter that translates the datatype of an AlphaChannelDataEmbedder instance to description
    /// </summary>
    public class DataTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return value;
            switch ((AlphaChannelDataEmbedder.DataTypeEnum)value)
            {
                case AlphaChannelDataEmbedder.DataTypeEnum.PlainText:
                    return "Image contains plain text data";
                case AlphaChannelDataEmbedder.DataTypeEnum.BinaryData:
                    return "Image contains binary data";
                case AlphaChannelDataEmbedder.DataTypeEnum.PlainTextEncrypted:
                    return "Image contains encrypted plain text data";
                case AlphaChannelDataEmbedder.DataTypeEnum.BinaryDataEncrypted:
                    return "Image contains encrypted binary data";
                default:
                    return "Image contains no embedded data";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }

    /// <summary>
    /// Converter to translate the value of an ImageSource (null or not null) to a visibility property
    /// </summary>
    public class ImageSourceToVisibilityAttributeConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value == null ? System.Windows.Visibility.Visible : System.Windows.Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }
}
