﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AlphaMessage
{
    /// <summary>
    /// Interaction logic for PasswordDialog.xaml
    /// </summary>
    public partial class PasswordDialog : Window
    {
        public string Password
        {
            get { return passwordBox.Password; }
        }

        public PasswordDialog()
        {
            InitializeComponent();
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            passwordBox.Focus();
        }

        // Check if the user gave a non-empty/whitespace password when closing the dialog
        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            //this.DialogResult = passwordBox.Password != "" ? true : false;
            this.DialogResult = String.IsNullOrWhiteSpace(passwordBox.Password) ? false : true;
        }
    }
}
